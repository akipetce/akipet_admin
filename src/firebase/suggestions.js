import * as firebase from "firebase";

/*
  Fetch all user suggestions
*/

export const fetchAllSuggestions = async place => {
  let error = "";
  let suggestions = [];

  try {
    await firebase
      .database()
      .ref("suggestions")
      .on("value", snapshot => {
        if (snapshot.val() !== null) {
          suggestions = Object.values(snapshot.val());
        }
      });
  } catch (err) {
    console.log(err);
    error = err;
  }
  return {
    suggestions,
    error
  };
};
