import * as firebase from "firebase";

/*
  Authenticate user with email and password
*/

export const login = async (email, password) => {
  let login_response = {};
  let error = "";
  let isAuthenticated = false;

  try {
    await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        isAuthenticated = true;
        login_response = response;
      })
      .catch(err => {
        error = err;
        login_response = false;
      });
  } catch (err) {
    error = err;
  }
  return {
    isAuthenticated,
    login_response,
    error
  };
};

/*
  Verify if user is already logged
*/

export const authenticated = async (email, password) => {
  let error = "";
  let isAuthenticated = false;

  try {
    await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        isAuthenticated = true;
      }
    });
  } catch (err) {
    error = err;
  }
  return {
    error,
    isAuthenticated
  };
};
