  export const dates = [
    {text: 'Todas Ações', value: null },
    {text: 'Mais Recentes', value: 'younger' },
    {text: 'Mais Antigas', value: 'older' },
  ];
  
  export const status = [
    {text: 'Pesquisar por', value: null },
    {text: 'Pendente', value: 'pending' },
    {text: 'Respondido', value: 'answered' },
  ];

  export const sender = [
    {text: 'Pesquisar por', value: null },
    {text: 'Síndico', value: 'sindico' },
    {text: 'Morador', value: 'morador' },
  ];