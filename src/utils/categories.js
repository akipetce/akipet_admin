export const categories = [
  { key: 1, section: "Selecione", value: null },
  { key: 2, section: "Hospedagem", value: "Hospedagem" },
  { key: 3, section: "Restaurante", value: "Restaurante" },
  { key: 4, section: "Lazer", value: "Lazer" },
  { key: 5, section: "Pet shop", value: "Pet shop" },
  { key: 6, section: "Local de trabalho", value: "Local de trabalho" },
  { key: 7, section: "Hospital 24h", value: "Hospital 24h" }
];
