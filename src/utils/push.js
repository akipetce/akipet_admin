export const pushMessages = {
  newPartner: {
    title: "Um novo parceiro foi adicionado",
    message:
      "Adicionamos um novo parceiro ao Akipet. Vai ficar de fora? venha conferir"
  },
  olhaAki: {
    title: "OlhaAki - novo post no ar",
    message: "Acabou de sair mais um post do OlhaAki com dicas e novidades PET"
  },
  voucher: {
    title: "Novos vouchers",
    message: "Nosso parceiro acabou de lançar um novo voucher. Vem conferir"
  }
};
