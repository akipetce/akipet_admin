const navigation = [
  {
    name: "Lugares",
    url: "/places",
    role: "admin"
  },
  {
    name: "Sugestões",
    url: "/suggestions",
    role: "admin"
  },
  {
    name: "Avaliações",
    url: "/evaluations",
    role: "admin"
  },
  {
    name: "OlhaAki",
    url: "/blog",
    role: "admin"
  },
  {
    name: "Vouchers",
    url: "/vouchers",
    role: "admin"
  },

  // Partner navbar
  { name: "Vouchers", url: "/voucher_validation", role: "partner" }
];

export { navigation };
