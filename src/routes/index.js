import React from "react";
import Loadable from "react-loadable";
import { isMobile } from "react-device-detect";
import { BrowserRouter as Router } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import { Route, Switch } from "react-router-dom";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Pages
const Login = Loadable({
  loader: () => import("../views/Pages/Login/index"),
  loading
});

const Places = Loadable({
  loader: () => import("../views/Places/index"),
  loading
});

const Evaluations = Loadable({
  loader: () => import("../views/Evaluations/index"),
  loading
});

const SuggestionsContainer = Loadable({
  loader: () => import("../views/Suggestions/index"),
  loading
});

const RegisterContainer = Loadable({
  loader: () => import("../views/Register/index"),
  loading
});

const RegisterPremium = Loadable({
  loader: () => import("../views/Register/index"),
  loading
});

const Blog = Loadable({
  loader: () => import("../views/Blog/index"),
  loading
});

const PartnerDashboard = Loadable({
  loader: () => import("../views/Partner/Dashboard/index"),
  loading
});

const VoucherContainer = Loadable({
  loader: () => import("../views/Vouchers/index"),
  loading
});

const Page404 = Loadable({
  loader: () => import("../views/Pages/Page404"),
  loading
});

const Page500 = Loadable({
  loader: () => import("../views/Pages/Page500"),
  loading
});

// -- START -- PRIVATE ROUTES

const PrivateRoute = ({ component, ...rest }) => {
  const isValid = localStorage.getItem("@token_valid");
  return <Route {...rest} component={isValid ? component : Login} />;
};

const PartnerRoute = ({ component, ...rest }) => {
  const isPartner = localStorage.getItem("@user_role") === "partner";
  return <Route {...rest} component={isPartner ? component : Login} />;
};

// -- END - PRIVATE ROUTES

const notify = (param, message) => {
  toast.configure({
    autoClose: 100000,
    draggable: false
  });
  toast[param](message);
};

const Routes = () => (
  <>
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        {!isMobile ? (
          <>
            <Route exact path="/" name="Login Page" component={Login} />
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route exact path="/404" name="Page 404" component={Page404} />
            <Route exact path="/500" name="Page 500" component={Page500} />
            <PrivateRoute
              exact
              path="/places"
              name="Lugares"
              component={Places}
            />
            <PrivateRoute
              exact
              path="/suggestions"
              name="Sugestões"
              component={SuggestionsContainer}
            />
            <PrivateRoute
              exact
              path="/evaluations"
              name="Avaliações"
              component={Evaluations}
            />
            <PrivateRoute exact path="/blog" name="OlhaAki" component={Blog} />
            <PrivateRoute
              exact
              path="/vouchers"
              name="Vouchers"
              component={VoucherContainer}
            />
            {/* Partner routes */}
            <PartnerRoute
              exact
              path="/voucher_validation"
              name="Área do parceiro"
              component={PartnerDashboard}
            />
            {/* END - Partner routes */}
            <Route
              exact
              path="/parceiro"
              name="Registre-se"
              component={RegisterContainer}
            />
            <Route
              exact
              path="/parceiro_premium"
              name="Registre-se"
              component={RegisterPremium}
            />
          </>
        ) : (
          notify("error", "Acesse este painel pelo seu computador")
        )}
      </Switch>
    </Router>
    <ToastContainer />
  </>
);

export default Routes;
