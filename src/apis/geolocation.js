import { unMask } from "../utils/mask";
import { MAPS } from "../utils/keys";

export const fetchCEP = async (cep, numero) => {
  let address = {};
  await fetch(`https://viacep.com.br/ws/${unMask(cep)}/json/`)
    .then(response => response.json())
    .then(async response => {
      if (response) {
        address = response;
        const { logradouro, localidade, uf } = address;
        address = await fetchGeolocation(
          address,
          logradouro,
          localidade,
          uf,
          numero
        );
      }
      console.log("cep response here", response);
    })
    .catch(err => {
      console.log("cep err", err);
    });
  return address;
};

export const fetchGeolocation = async (
  address,
  logradouro,
  localidade,
  uf,
  numero
) => {
  let geolocation = {};
  await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${numero}+${logradouro},
  +${localidade},+${uf}&key=${MAPS}`)
    .then(response => response.json())
    .then(async response => {
      geolocation = {
        latitude: response.results[0].geometry.location.lat,
        longitude: response.results[0].geometry.location.lng
      };
      console.log("fetch geolocation response here", response);
    })
    .catch(err => {
      console.log("fetch geolocation err", err);
    });
  return { geolocation, address };
};
