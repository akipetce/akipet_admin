// Api endpoints
const version = "version";
const login = `auth/authenticate`;
const register = `auth/register`;
const places_endpoint = `places`;
const forgot_password = `auth/forgot_password`;
const reset_password = `auth/reset_password`;
const comments = `comments`;
const ratings = `ratings`;
const indications = `indications`;
const upload = `upload`;
const partner = `partner`;
const blog_endpoint = `blogs`;
const voucher_endpoint = `vouchers`;
const create_partner = `auth/register`;
const push_notification = `push_notification`;

export {
  version,
  login,
  register,
  places_endpoint,
  forgot_password,
  reset_password,
  blog_endpoint,
  comments,
  ratings,
  indications,
  upload,
  partner,
  voucher_endpoint,
  create_partner,
  push_notification
};
