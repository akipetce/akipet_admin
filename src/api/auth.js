import api from "./";

export const TOKEN_KEY = "@akipet-key";
export const TOKEN_VALID = "@token_valid";
export const USER_ROLE = "@user_role";

export const isAuthenticated = async () => {
  const token = localStorage.getItem(TOKEN_KEY);

  if (token === null) return false;

  let isValid = await api("get", token, {}, true);
  if (isValid && isValid.status === 200) isValid = true;
  localStorage.setItem(TOKEN_VALID, isValid);
};

export const setRole = async role => {
  localStorage.setItem(USER_ROLE, role);
};

export const getToken = () => localStorage.getItem(TOKEN_KEY);
export const getRole = () => localStorage.getItem(USER_ROLE);

export const setKey = token => {
  localStorage.setItem(TOKEN_KEY, token);
};

export const removeKey = () => {
  localStorage.removeItem(TOKEN_KEY);
};
export const removeRole = () => {
  localStorage.removeItem(USER_ROLE);
};
export const removeValidation = () => {
  localStorage.removeItem(TOKEN_VALID);
};
