import * as firebase from "firebase";
import uuid from "uuid/v1";

const storage = firebase.storage();
const storageRef = storage.ref();

var metadata = {
  contentType: "image/*"
};

export const upload = async (file, action) => {
  const id = uuid();
  let url = "";
  var uploadTask = await storageRef.child(id).put(file);
  console.log("upload task", uploadTask, file);
  if (uploadTask) {
    await storageRef
      .child(id)
      .getDownloadURL()
      .then(address => (url = address))
      .catch(err => console.log("get url error", err));
  }
  console.log("url", url);
  return url;
};
