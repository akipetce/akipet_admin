import axios from "axios";
import { login } from "./endpoints";

const api = async (method, param, data = {}, validation) => {
  const url = `https://akipet-api.herokuapp.com/`;
  // const url = `http://localhost:3001/`;
  const token = localStorage.getItem("@akipet-key");

  if (param === login) {
    return await axios({
      method: method.toLowerCase(),
      url: `${url}${param}`,
      data
    });
  }

  if (method.toLowerCase() === "get" && !validation) {
    return await axios({
      method,
      url: `${url}${param}`,
      headers: {
        authorization: `Bearer ${token}`
      }
    });
  }

  if (validation) {
    return await axios({
      method: "get",
      url: `${url}projects`,
      headers: {
        authorization: `Bearer ${token}`
      }
    });
  }

  return {
    post: await axios({
      method: method.toLowerCase(),
      url: `${url}${param}`,
      data,
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  };
};

export default api;
