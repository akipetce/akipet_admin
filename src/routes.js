import React from "react";

const Login = React.lazy(() => import("./views/Pages/Login/Login"));
const Places = React.lazy(() => import("./views/Places"));
const Users = React.lazy(() => import("./views/Users"));
const Evaluations = React.lazy(() => import("./views/Evaluations"));

const routes = [
  { path: "/login", name: "Login", component: Login },
  { path: "/places", name: "Lugares", component: Places },
  { path: "/users", name: "Usuários", component: Users },
  { path: "/evaluations", name: "Avaliações", component: Evaluations }
];

export default routes;
