import React from "react";
import { Loader, Dimmer } from "semantic-ui-react";

const Loading = () => (
  <Dimmer active>
    <Loader size="medium" inline="centered" />
  </Dimmer>
);

export default Loading;
