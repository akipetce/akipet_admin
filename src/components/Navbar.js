import React from "react";
import { navigation } from "../_nav";
import { withRouter } from "react-router-dom";

// css
import "../assets/css/navbar.css";
import { removeKey, removeRole, removeValidation } from "../api/auth";

const Navbar = props => {
  const handleNavigate = url => props.history.push(url);
  const role = localStorage.getItem("@user_role");
  const navItems = () => {
    const { pathname } = props.location;
    return (
      <div className="bar-container">
        {navigation.map((nav, index) => {
          if (nav.role === role) {
            return (
              <div
                key={index}
                className={"icon-bar-container"}
                onClick={() => handleNavigate(nav.url)}
              >
                <b
                  className={
                    pathname === nav.url ? "icon-title-selected" : "icon-title"
                  }
                >
                  {nav.name}
                </b>
              </div>
            );
          }
        })}
      </div>
    );
  };

  const logout = () => {
    removeRole();
    removeKey();
    removeValidation();
    props.history.push("/login");
  };

  return (
    <>
      <div className="navbar-container">
        <img
          alt="logo"
          className="logo-bar"
          src={require("../assets/img/logo.png")}
        />
        {navItems()}
      </div>
      <div onClick={logout}>
        <b className="logout-bar-title">Sair</b>
      </div>
    </>
  );
};

export default withRouter(Navbar);
