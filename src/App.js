import React, { Component } from "react";
import { registerPlugin } from "react-filepond";
import { Provider } from "react-redux";
import store from "./store";
import { firebaseConfig } from "./firebase/config";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import FilePondPluginFileEncode from "filepond-plugin-file-encode";

import Routes from "./routes/index";

import * as firebase from "firebase";

import "./App.scss";
import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import "react-toastify/dist/ReactToastify.css";

class App extends Component {
  componentWillMount() {
    require("dotenv").config();
    registerPlugin(FilePondPluginImagePreview, FilePondPluginFileEncode);
    !firebase.apps.length
      ? firebase.initializeApp(firebaseConfig())
      : firebase.app();
  }

  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}

export default App;
