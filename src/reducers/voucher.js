import {
  MODAL_OPEN,
  LOADING_PROGRESS,
  MODAL_CLOSE,
  FETCH_VOUCHER_SUCCESS,
  FETCH_VOUCHER_FAILURE,
  CREATE_VOUCHER_FAILURE,
  UPDATE_VOUCHER_FAILURE
} from "../actions/actionTypes";

const initialState = {
  vouchers: [],
  failed: false,
  loading: false,
  openModal: false,
  isAuthenticated: false
};

export const vouchers = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PROGRESS:
      return {
        ...state,
        loading: true
      };
    case MODAL_OPEN:
      return {
        ...state,
        openModal: true
      };
    case MODAL_CLOSE:
      return {
        ...state,
        openModal: false
      };
    case FETCH_VOUCHER_SUCCESS:
      return {
        ...state,
        vouchers: action.payload,
        loading: false
      };
    case FETCH_VOUCHER_FAILURE:
      return {
        ...state,
        failed: true,
        loading: false
      };
    default:
      return state;
  }
};
