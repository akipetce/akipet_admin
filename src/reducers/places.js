import {
  MODAL_OPEN,
  MODAL_CLOSE,
  LOADING_PROGRESS,
  FETCH_PLACES_SUCCESS,
  FETCH_PLACES_FAILURE,
  CREATE_PLACES_SUCCESS,
  CREATE_PLACES_FAILURE,
  UPDATE_PLACES_SUCCESS,
  UPDATE_PLACES_FAILURE,
  DELETE_PLACES_SUCCESS,
  DELETE_PLACES_FAILURE
} from "../actions/actionTypes";

const initialState = {
  places: [],
  failed: false,
  loading: false,
  openModal: false,
  isAuthenticated: false
};

export const places = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PROGRESS:
      return {
        ...state,
        loading: true
      };
    case MODAL_OPEN:
      return {
        ...state,
        openModal: true
      };
    case MODAL_CLOSE:
      return {
        ...state,
        openModal: false
      };
    case FETCH_PLACES_SUCCESS:
      return {
        ...state,
        places: action.payload,
        loading: false
      };
    case FETCH_PLACES_FAILURE:
      return {
        ...state,
        failed: true,
        loading: false
      };
    case CREATE_PLACES_SUCCESS:
      return {
        ...state,
        places: action.payload
      };
    case CREATE_PLACES_FAILURE:
      return {
        ...state,
        place: {},
        openModal: true
      };
    case UPDATE_PLACES_SUCCESS:
      return {
        ...state,
        place: action.payload
      };
    case UPDATE_PLACES_FAILURE:
      return {
        ...state,
        openModal: true,
        place: {}
      };
    case DELETE_PLACES_SUCCESS:
      return {
        ...state,
        loading: false,
        failed: false
      };
    case DELETE_PLACES_FAILURE:
      return {
        ...state,
        loading: false,
        openModal: true,
        failed: true
      };
    default:
      return state;
  }
};
