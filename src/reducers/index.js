import { combineReducers } from "redux";

import { auth } from "./auth";
import { places } from "./places";
import { comments } from "./comments";
import { blogs } from "./blog";
import { vouchers } from "./voucher";
import { user } from "./user";

export default combineReducers({
  auth,
  places,
  comments,
  blogs,
  vouchers,
  user
});
