import {
  MODAL_OPEN,
  LOADING_PROGRESS,
  MODAL_CLOSE,
  FETCH_BLOG_SUCCESS,
  FETCH_BLOG_FAILURE,
  CREATE_BLOG_FAILURE,
  UPDATE_BLOG_FAILURE
} from "../actions/actionTypes";

const initialState = {
  blogs: [],
  failed: false,
  loading: false,
  openModal: false,
  isAuthenticated: false
};

export const blogs = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PROGRESS:
      return {
        ...state,
        loading: true
      };
    case MODAL_OPEN:
      return {
        ...state,
        openModal: true
      };
    case MODAL_CLOSE:
      return {
        ...state,
        openModal: false
      };
    case FETCH_BLOG_SUCCESS:
      return {
        ...state,
        blogs: action.payload,
        loading: false
      };
    case FETCH_BLOG_FAILURE:
      return {
        ...state,
        failed: true,
        loading: false
      };
    default:
      return state;
  }
};
