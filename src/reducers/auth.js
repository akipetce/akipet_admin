import {
  LOGIN_AUTHENTICATION_SUCCESS,
  LOGIN_AUTHENTICATION_FAILURE
} from "../actions/actionTypes";

const initialState = {
  isAuthenticated: false,
  loading: false,
  failed: false
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_AUTHENTICATION_SUCCESS:
      return {
        ...state,
        isAuthenticated: action.payload,
        loading: false,
        failed: false
      };
    case LOGIN_AUTHENTICATION_FAILURE:
      return {
        ...state,
        loading: false,
        failed: true
      };
    default:
      return state;
  }
};
