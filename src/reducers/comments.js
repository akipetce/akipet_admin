import {
  MODAL_CLOSE,
  MODAL_OPEN,
  LOADING_PROGRESS,
  FETCH_COMMENTS_SUCESS,
  FETCH_COMMENTS_FAILURE,
  DELETE_COMMENT_SUCESS
} from "../actions/actionTypes";

const initialState = {
  allComments: [],
  failed: false,
  loading: false,
  openModal: false,
  isAuthenticated: false
};

export const comments = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PROGRESS:
      return {
        ...state,
        loading: true
      };
    case MODAL_OPEN:
      return {
        ...state,
        openModal: true
      };
    case MODAL_CLOSE:
      return {
        ...state,
        openModal: false
      };
    case FETCH_COMMENTS_SUCESS:
      return {
        ...state,
        allComments: action.payload,
        loading: false
      };
    case FETCH_COMMENTS_FAILURE:
      return {
        ...state,
        failed: true,
        loading: false
      };
    default:
      return state;
  }
};
