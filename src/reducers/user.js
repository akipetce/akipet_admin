import { SET_USER, REMOVE_USER } from "../actions/actionTypes";

const initialState = {
  user: {}
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.payload
      };
    case REMOVE_USER:
      return {
        ...state,
        user: {}
      };
    default:
      return state;
  }
};
