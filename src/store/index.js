import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducer from "../reducers";
import { saveState, loadState } from "./localStorage";

import { createStore, applyMiddleware, compose, combineReducers } from "redux";

const middlewares = [thunk];
middlewares.push(logger);

const persistedStaste = loadState();

const store = createStore(
  rootReducer,
  persistedStaste,
  compose(applyMiddleware(...middlewares))
);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
