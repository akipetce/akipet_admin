import React from "react";

// reactstrap components
import { Container } from "reactstrap";

// Login component
import Suggestions from "./Suggestions";

// css
import "../../assets/css/index.css";
import Navbar from "../../components/Navbar";

class SuggestionsContainer extends React.Component {
  componentDidMount() {
    document.body.classList.add("gradient");
  }
  componentWillUnmount() {
    document.body.classList.remove("gradient");
  }
  render() {
    return (
      <>
        <Navbar />
        <div className="gradient">
          <div className="py-8"></div>
          <Container className="mt--8 pb-5">
            <Suggestions />
          </Container>
        </div>
      </>
    );
  }
}

export default SuggestionsContainer;
