import React, { Component } from "react";
import { Card, CardBody, Table, FormGroup, Input, Row, Col } from "reactstrap";

import { connect } from "react-redux";
import { IconButton } from "evergreen-ui/commonjs/buttons";
import { toast, ToastContainer } from "react-toastify";

// utils, api && database
import { fetchAllSuggestions } from "../../firebase/suggestions";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";

class Suggestions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestions: []
    };
  }

  async componentDidMount() {
    const suggestions = await fetchAllSuggestions();
    if (suggestions) {
      this.setState({ suggestions: suggestions.suggestions });
    }
  }

  handleToggleModal = content => {
    if (content) {
      const {
        id,
        email,
        place,
        folder,
        mobile,
        number,
        photos,
        address,
        latitude,
        longitude,
        telephone,
        description,
        timeAttendance
      } = content;

      this.setState({
        id,
        address,
        place,
        folder,
        number,
        description,
        timeAttendance,
        telephone,
        mobile,
        email,
        latitude,
        longitude,
        photos,
        openModal: true
      });
    } else {
      this.setState({ openModal: true });
    }
  };

  notify = (param, message) => toast[param](message);

  suggestionsRow = suggestion => {
    return (
      <tr key={suggestion.id}>
        <td>{`${suggestion.address}}`}</td>
        <td>{suggestion.category}</td>
        <td>{suggestion.email}</td>
        <td>{suggestion.address}</td>
        <td>{suggestion.contact}</td>
        <td>{suggestion.email}</td>
        <td>
          <IconButton
            icon="edit"
            onClick={() => this.handleToggleModal(suggestion)}
          ></IconButton>
        </td>
      </tr>
    );
  };

  render() {
    const { suggestions } = this.state;
    console.log(suggestions);
    return (
      <div>
        <div className="row-1">
          <Row form>
            <Col md={9}>
              <FormGroup>
                <b className="subtitle">Pesquisar por:</b>
                <Input
                  name="search"
                  onChange={text => this.setState({ text: text.target.value })}
                  placeholder="Pesquise aqui"
                />
              </FormGroup>
            </Col>
            <Col md={3}>
              <button
                className="add-button bt-1"
                onClick={() => this.filter(null)}
              >
                Buscar
              </button>
            </Col>
          </Row>
        </div>
        <Row>
          <Col xl={12}>
            <Card>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">Local</th>
                      <th scope="col">Categoria</th>
                      <th scope="col">Endereço</th>
                      <th scope="col">Contato</th>
                      <th scope="col">E-mail</th>
                    </tr>
                  </thead>
                  <tbody>
                    {suggestions &&
                      suggestions.map(suggestion =>
                        this.suggestionsRow(suggestion)
                      )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Suggestions);
