import React, { Component } from "react";
import {
  Card,
  CardBody,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import axios from "axios";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import Loading from "../../components/Loader";
import * as EmailValidator from "email-validator";
import { categories } from "../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../utils/mask";
import { upload } from "../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../apis/geolocation";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";
import {
  setLoading,
  setBlog,
  createBlog,
  updateBlog,
  deleteBlog
} from "../../actions/blog";
import { loader } from "../../actions/loader";
import { modalOpen, closeModal } from "../../actions/modal";

class Blog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      title: "",
      content: "",
      folder: {},
      blogs: [],
      blogFiltered: []
    };
  }

  async componentDidMount() {
    await this.props.setLoading();
    await this.props.setBlog();
  }

  async componentWillReceiveProps(nextProps) {
    const { blogs } = nextProps;
    if (blogs && blogs.length > 0) {
      this.setState({ blogs: blogs });
    }
  }

  deleteBlog = async id => {
    const blogs = this.props.blogs;
    await this.props.deleteBlog(id, blogs);
    await this.props.setBlogs();
    return this.notify(`${"success"}`, `${`AkiNotícia deletado com sucesso`}`);
  };

  dropdownToogle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  filterBlog = text => {
    const input = text.target.value;
    const { blogs } = this.state;
    const blogFiltered = _.filter(
      blogs,
      e =>
        e.title.toLowerCase().includes(input.toLowerCase()) ||
        e.content.toLowerCase().includes(input.toLowerCase())
    );
    this.setState({ blogFiltered });
  };

  handleToggleModal = content => {
    if (content) {
      this.setState({
        id: content._id,
        folder: content.folder,
        title: content.title,
        content: content.content
      });
    }
    this.props.modalOpen();
  };

  notify = (param, message) => toast[param](message);

  onChangeHandler = async (event, key, index) => {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    this.setState({ [key]: { file } }, () => {
      reader.onload = async () => {
        let photos = this.state.photos;
        if (key === "folder") {
          this.setState(prevState => ({
            [key]: { file, preview: reader.result }
          }));
        } else {
          photos[index] = reader.result;
          this.setState(prevState => ({
            photos,
            [key]: { file, preview: reader.result }
          }));
        }
      };
    });
    reader.readAsDataURL(file);
  };

  blogsRow = (blog, index) => {
    if (blog && blog.title) {
      return (
        <tr key={index} onClick={() => this.handleToggleModal(blog)}>
          <td>
            <img src={blog.folder} alt="folder" className="folder" />
          </td>
          <td>{blog.title}</td>
        </tr>
      );
    }
  };

  renderModal = () => {
    const { id, folder, title, content } = this.state;

    const isUpdate = id && id.length > 0;

    return (
      <Modal isOpen={this.props.openModal} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>
          {isUpdate ? "Atualizar" : "Novo"} OlhaAki
        </ModalHeader>
        <div className="file-upload">
          <div className="image-upload-wrap">
            <input
              className="file-upload-input"
              type="file"
              onChange={file => this.onChangeHandler(file, `folder`)}
              accept="image/*"
            />
            {isUpdate ? (
              <img
                className="file-upload-image"
                src={this.state.folder}
                alt="folder"
              />
            ) : this.state.folder &&
              this.state.folder.preview &&
              this.state.folder.preview.length > 0 ? (
              <img
                className="file-upload-image"
                src={this.state.folder.preview}
                alt="folder"
              />
            ) : (
              <div className="drag-text">
                <h3>Adicione uma imagem</h3>
              </div>
            )}
          </div>
        </div>
        <ModalBody>
          <div>
            <b>Título</b>{" "}
            <Input
              type="text"
              placeholder="Título"
              onChange={title =>
                this.setState({
                  title: title.target.value
                })
              }
              value={title}
            />{" "}
            <b>Conteúdo</b>
            <Input
              type="textarea"
              placeholder="Escreva o conteúdo do post aqui - Observação: mínimo 50 caracteres"
              onChange={content =>
                this.setState({
                  content: content.target.value
                })
              }
              value={content}
            />{" "}
          </div>
        </ModalBody>
        <ModalFooter>
          {" "}
          {isUpdate && (
            <Button
              color="danger"
              onClick={() => {
                if (
                  window.confirm(
                    "Atenção, a ação de excluir é irreversível. Você confirma essa ação?"
                  )
                )
                  this.handleDelete(id);
              }}
            >
              Apagar
            </Button>
          )}
          <Button color="secondary" onClick={() => this.handleCancel()}>
            Cancelar
          </Button>{" "}
          <Button color="success" onClick={event => this.validateFields(event)}>
            {isUpdate ? "Atualizar" : "Criar"}
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  saveBlog = async () => {
    const { id, title, content, folder } = this.state;

    const isUpdate = id && id.length > 0;
    let isSave = false;
    let urls = [];

    const blogToSave = {
      title,
      content
    };

    await loader();
    let folderUrl = "";

    if (!isUpdate) {
      await Promise.all(
        [folder].map(async (file, index) => {
          const url = await upload(file.file);
          if (index === 0) {
            folderUrl = url;
          }
        })
      );
    }

    if (isUpdate) {
      isSave = await this.props.updateBlog(blogToSave, id);
    } else {
      isSave = await this.props.createBlog({
        ...blogToSave,
        folder: folderUrl
      });
    }

    await this.props.setBlog();
    isSave && this.resetState();

    this.notify(
      `${isSave ? "success" : "danger"}`,
      `${
        isSave
          ? `OlhaAki ${
              isUpdate ? "atualizado" : "criado"
            } com sucesso. Uma notificação foi enviada para todos os usuários do app`
          : "Erro interno de servidor"
      }`
    );
  };

  resetState = () => {
    this.setState({
      id: "",
      title: "",
      content: "",
      folder: ""
    });
  };

  validateFields = async event => {
    const { id, title, content, folder } = this.state;

    let isFolderPreview =
      id && id.length > 0
        ? false
        : !(folder && folder.preview && folder.preview.length > 0);

    if (
      !(
        !isFolderPreview &&
        title &&
        title.length > 0 &&
        content &&
        content.length > 10
      )
    ) {
      this.notify(
        "error",
        "Verifique se todos os campos foram preenchidos corretamente"
      );
      return;
    }
    return this.saveBlog();
  };

  handleCancel = () => {
    this.props.closeModal();
    this.resetState();
  };

  handleDelete = id => {
    this.resetState();
    this.deleteBlog(id);
  };

  render() {
    const { loading } = this.props;
    const { blogs, blogFiltered } = this.state;
    if (loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className="row-1">
            <Row form>
              <Col md={9}>
                <FormGroup>
                  <b className="subtitle">Pesquisar por:</b>
                  <Input
                    name="search"
                    onChange={this.filterBlog}
                    placeholder="Pesquise aqui por local"
                  />
                </FormGroup>
              </Col>
              <Col md={3}>
                <button
                  className="add-button bt-1"
                  onClick={() => this.handleToggleModal(null)}
                >
                  Adicionar
                </button>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xl={12}>
              <Card>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Folder</th>
                        <th scope="col">Título</th>
                      </tr>
                    </thead>
                    <tbody>
                      {(blogFiltered && blogFiltered.length > 0
                        ? blogFiltered
                        : blogs
                      ).map((blog, index) => this.blogsRow(blog, index))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.renderModal()}
          <ToastContainer />
        </div>
      );
    }
  }
}

const mapDispatchToProps = {
  setLoading,
  setBlog,
  createBlog,
  updateBlog,
  deleteBlog,
  modalOpen,
  closeModal,
  loader
};

const mapStateToProps = state => ({
  openModal: state.places.openModal,
  loading: state.blogs.loading,
  blogs: state.blogs.blogs
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog);
