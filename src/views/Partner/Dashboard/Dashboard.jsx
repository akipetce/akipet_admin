import React, { Component } from "react";
import {
  Card,
  CardBody,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import axios from "axios";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import Loading from "../../../components/Loader";
import * as EmailValidator from "email-validator";
import { categories } from "../../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../../utils/mask";
import { upload } from "../../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../../apis/geolocation";

// css
import "../../../assets/css/index.css";
import "../../../assets/css/upload.css";
import "../../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";
import {
  setVouchers,
  setLoading,
  updateVoucher
} from "../../../actions/voucher";
import { loader } from "../../../actions/loader";
import { modalOpen, closeModal } from "../../../actions/modal";

class PartnerDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      voucherFiltered: [],
      vouchers: [],
      vouchersUsed: [],
      voucher: ""
    };
  }

  async componentDidMount() {
    await this.props.setLoading();
    await this.props.setVouchers();
  }

  async componentWillReceiveProps(nextProps) {
    const { vouchers, user } = nextProps;
    if (vouchers && vouchers.length > 0 && user && user._id) {
      const placeVoucher = _.filter(
        vouchers,
        e => e.place._id === user.place._id
      );
      const vouchersUsed = _.filter(
        placeVoucher,
        e => e.voucherUsed === "true"
      );
      this.setState({ vouchers: placeVoucher, vouchersUsed });
    }
  }

  filterVoucher = text => {
    const input = text.target.value;
    const { vouchersUsed } = this.state;
    const voucherFiltered = _.filter(vouchersUsed, e =>
      e.voucher.toLowerCase().includes(input.toLowerCase())
    );
    this.setState({ voucherFiltered });
  };

  notify = (param, message) => toast[param](message);

  vouchersRow = (voucher, index) => {
    if (voucher && voucher._id) {
      return (
        <tr key={index}>
          <td>{voucher.voucher}</td>
          <td>
            {`${voucher.voucherValue}
            ${voucher.voucherType === "porcentagem" ? "%" : "reais"}`}
          </td>
          <td>{voucher.voucherExpiration}</td>
          <td>{voucher.voucherUsed ? "Resgatado" : "Disponível"}</td>
        </tr>
      );
    }
  };

  handleToggleModal = () => this.props.modalOpen();

  renderModal = () => {
    const { voucher } = this.state;

    return (
      <Modal isOpen={this.props.openModal} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>Validar voucher</ModalHeader>
        <ModalBody>
          <div>
            <b>Voucher</b>{" "}
            <Input
              type="text"
              placeholder="Insira o voucher"
              onChange={voucher =>
                this.setState({
                  voucher: voucher.target.value
                })
              }
              value={voucher}
            />{" "}
          </div>
        </ModalBody>
        <ModalFooter>
          {" "}
          <Button color="secondary" onClick={() => this.props.closeModal()}>
            Cancelar
          </Button>{" "}
          <Button color="success" onClick={event => this.validateFields(event)}>
            Validar
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  validateVoucher = async () => {
    const { voucher, vouchers } = this.state;
    const isVoucher = _.filter(vouchers, e => e.voucher === voucher);
    console.log("is voucher", isVoucher, voucher, vouchers);
    if (isVoucher && isVoucher.length > 0) {
      if (isVoucher[0].voucherUsed === "false") {
        const isSave = await this.props.updateVoucher(
          isVoucher[0],
          isVoucher[0]._id
        );
        isSave && this.resetState();

        this.notify(
          `${isSave ? "success" : "error"}`,
          `${
            isSave
              ? `Voucher validado com sucesso!`
              : "Erro interno de servidor"
          }`
        );
      } else {
        this.notify("error", "Atenção: voucher já resgatado");
      }
    } else {
      this.notify(
        "error",
        "Atenção: voucher inexistente na base de dados ou não pertencente a este estabelecimento"
      );
    }
  };

  resetState = () => {
    this.setState({
      vouchers: [],
      voucher: "",
      voucherFiltered: []
    });
  };

  validateFields = async event => {
    const { voucher } = this.state;
    console.log(voucher.length);
    if (voucher.length === 0) {
      this.notify(
        "error",
        "Verifique se todos os campos foram preenchidos corretamente"
      );
      return;
    }
    this.validateVoucher();
  };

  render() {
    console.log("user aqui", this.props.user);
    const { loading } = this.props;
    const { vouchers, voucherFiltered, vouchersUsed } = this.state;
    if (loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className="row-1">
            <Row form>
              <Col md={9}>
                <FormGroup>
                  <b className="subtitle">Pesquisar por:</b>
                  <Input
                    name="search"
                    onChange={this.filterVoucher}
                    placeholder="Pesquise aqui por um voucher"
                  />
                </FormGroup>
              </Col>
              <Col md={3}>
                <button
                  className="add-button bt-1"
                  onClick={() => this.handleToggleModal()}
                >
                  Validar
                </button>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xl={12}>
              <Card>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Voucher</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Validade</th>
                        <th scope="col">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      {(voucherFiltered && voucherFiltered.length > 0
                        ? voucherFiltered
                        : vouchersUsed
                      ).map((voucher, index) =>
                        this.vouchersRow(voucher, index)
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.renderModal()}
          <ToastContainer />
        </div>
      );
    }
  }
}

const mapDispatchToProps = {
  setLoading,
  setVouchers,
  modalOpen,
  closeModal,
  loader,
  updateVoucher
};

const mapStateToProps = state => ({
  openModal: state.vouchers.openModal,
  loading: state.vouchers.loading,
  vouchers: state.vouchers.vouchers,
  user: state.user.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PartnerDashboard);
