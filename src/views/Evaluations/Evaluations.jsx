import React, { Component } from "react";
import {
  Card,
  CardBody,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Button,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import axios from "axios";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import Loading from "../../components/Loader";
import * as EmailValidator from "email-validator";
import { categories } from "../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../utils/mask";
import { upload } from "../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../apis/geolocation";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";
import {
  setLoading,
  createPlaces,
  updatePlaces,
  deletePlace,
  setPlaces
} from "../../actions/places";
import { setComments, deleteComment } from "../../actions/comments";
import { loader } from "../../actions/loader";
import { modalOpen, closeModal } from "../../actions/modal";

class Evaluations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      placeFiltered: [],
      places: [],
      address: {},
      place: "",
      folder: {},
      number: "",
      description: "",
      timeAttendance: "",
      telephone: "",
      mobile: "",
      email: "",
      cep: "",
      conduction: "",
      specie: "",
      size: "",
      area: "",
      maskedCep: "",
      placeSelected: {},
      preview: {},
      latitude: null,
      longitude: null,
      photo0: {},
      photo1: {},
      photo2: {},
      photos: [null, null, null]
    };
  }

  async componentDidMount() {
    await this.props.setComments();
    await this.props.setPlaces();
  }

  async componentWillReceiveProps(nextProps) {
    const { places } = nextProps;
    if (places && places.comments && places.comments.length > 0) {
      this.setState({ places: places });
    }
  }

  filterPlace = text => {
    const input = text.target.value;
    const { places } = this.state;
    const placeFiltered = _.filter(
      places,
      e =>
        e.place.toLowerCase().includes(input.toLowerCase()) ||
        e.category.toLowerCase().includes(input.toLowerCase()) ||
        e.address.logradouro.toLowerCase().includes(input.toLowerCase()) ||
        e.email.toLowerCase().includes(input.toLowerCase()) ||
        e.address.localidade.toLowerCase().includes(input.toLowerCase())
    );
    this.setState({ placeFiltered });
  };

  handleToggleModal = content => {
    const allComments = this.props.comments;
    if (content) {
      const {
        id,
        cep,
        email,
        place,
        mobile,
        number,
        address,
        category,
        latitude,
        longitude,
        telephone,
        description,
        conduction,
        specie,
        size,
        area,
        folderUrl,
        photosUrl,
        timeAttendance
      } = content;

      const isPhotos = content && content.photo0 && content.photo0.length > 0;
      const placeComments = _.filter(
        allComments,
        e => e.place._id === content._id
      );
      console.log("place comments", placeComments);

      this.setState({
        id: content._id,
        cep,
        place,
        folder: content.folder,
        number,
        address,
        category,
        description,
        timeAttendance,
        telephone,
        mobile,
        email,
        latitude,
        longitude,
        folderUrl,
        photosUrl,
        conduction,
        specie,
        size,
        area,
        placeComments,
        photo0: isPhotos ? content.photo0 : "",
        photo1: isPhotos ? content.photo1 : "",
        photo2: isPhotos ? content.photo2 : ""
      });
    }
    this.props.modalOpen();
  };

  handleChange = address => {
    this.setState({ address });
  };

  handleDropdown = param => {
    this.setState({
      category: param,
      dropdownOpen: false
    });
  };

  handleCEP = async () => {
    const { cep, number } = this.state;

    const isValidCEP = cep.length === 10;
    const isValidNumber = number.length > 0;
    if (isValidCEP && isValidNumber) {
      const address = await fetchCEP(this.state.cep, number);
      if (address && Object.values(address).length > 0) {
        this.setState({
          address: address.address,
          latitude: address.geolocation.latitude,
          longitude: address.geolocation.longitude
        });
      } else {
        this.notify("error", "Erro interno de servidor");
      }
    } else {
      this.notify("error", "Por favor preencha CEP e número do endereço");
    }
  };

  notify = (param, message) => toast[param](message);

  onChangeHandler = async (event, key, index) => {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    this.setState({ [key]: { file } }, () => {
      reader.onload = async () => {
        let photos = this.state.photos;
        if (key === "folder") {
          this.setState(prevState => ({
            [key]: { file, preview: reader.result }
          }));
        } else {
          photos[index] = reader.result;
          this.setState(prevState => ({
            photos,
            [key]: { file, preview: reader.result }
          }));
        }
      };
    });
    reader.readAsDataURL(file);
  };

  placesRow = (place, index) => {
    if (place && place.address) {
      return (
        <tr key={index} onClick={() => this.handleToggleModal(place)}>
          <td>
            <img src={place.folder} alt="folder" className="folder" />
          </td>
          <td>{place.place}</td>
          <td>{`${place.address.logradouro}, ${place.number} - ${place.address.localidade}/${place.address.uf}`}</td>
          <td>{place.category}</td>
          <td>{place.email}</td>
        </tr>
      );
    }
  };

  renderModal = () => {
    const { id, place } = this.state;

    const isUpdate = id && id.length > 0;

    return (
      <Modal isOpen={this.props.openModal} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>Avaliações e comentários</ModalHeader>
        <div className="file-upload">
          <div className="image-upload-wrap">
            <input
              className="file-upload-input"
              type="file"
              onChange={file => this.onChangeHandler(file, `folder`)}
              accept="image/*"
            />
            {isUpdate ? (
              <img
                className="file-upload-image"
                src={this.state.folder}
                alt="folder"
              />
            ) : this.state.folder &&
              this.state.folder.preview &&
              this.state.folder.preview.length > 0 ? (
              <img
                className="file-upload-image"
                src={this.state.folder.preview}
                alt="folder"
              />
            ) : (
              <div className="drag-text">
                <h3>Adicione uma imagem</h3>
              </div>
            )}
          </div>
        </div>
        <ModalBody>
          <div>
            <Row form>
              <Col md={6}>
                <b>Nome do local</b>{" "}
                <Input
                  type="text"
                  placeholder="Nome do estabelecimento"
                  onChange={place =>
                    this.setState({
                      place: place.target.value
                    })
                  }
                  value={place}
                />{" "}
              </Col>
              <Col md={3}>
                <b>Avaliação</b>
                <Input
                  disabled
                  type="text"
                  placeholder="Número"
                  onChange={number => false}
                  value="5.00"
                />{" "}
              </Col>
            </Row>
            <b>Comentários</b>{" "}
            {this.state.placeComments &&
              this.state.placeComments.length > 0 &&
              this.state.placeComments.map(comment => (
                <div class="ui card">
                  <div class="content">
                    <div class="header">{comment.user.name}</div>
                    <div class="description">
                      <p>{comment.comment}</p>
                    </div>
                  </div>
                  <div class="extra content">
                    <span class="right floated star">
                      <div
                        class="ui vertical animated button"
                        onClick={() => this.deleteComment(comment._id)}
                        tabindex="0"
                      >
                        <div class="hidden content">Apagar</div>
                        <div class="visible content">
                          <i class="trash icon"></i>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              ))}
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => this.props.closeModal()}>
            Sair
          </Button>{" "}
        </ModalFooter>
      </Modal>
    );
  };

  deleteComment = async id => {
    await this.props.deleteComment(id);
    _.remove(this.state.placeComments, e => e._id === id);
    await this.props.setPlaces();
  };

  render() {
    const { loading } = this.props;
    const { places, placeFiltered } = this.state;
    if (loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className="row-1">
            <Row form>
              <Col md={12}>
                <FormGroup>
                  <b className="subtitle">Pesquisar por:</b>
                  <Input
                    name="search"
                    onChange={this.filterPlace}
                    placeholder="Pesquise aqui por local"
                  />
                </FormGroup>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xl={12}>
              <Card>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Folder</th>
                        <th scope="col">Estabelecimento</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">E-mail</th>
                      </tr>
                    </thead>
                    <tbody>
                      {(placeFiltered && placeFiltered.length > 0
                        ? placeFiltered
                        : places
                      ).map((place, index) => this.placesRow(place, index))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.renderModal()}
          <ToastContainer />
        </div>
      );
    }
  }
}

const mapDispatchToProps = {
  setLoading,
  setPlaces,
  setComments,
  createPlaces,
  updatePlaces,
  deletePlace,
  modalOpen,
  closeModal,
  deleteComment,
  loader
};

const mapStateToProps = state => ({
  openModal: state.places.openModal,
  loading: state.places.loading,
  places: state.places.places,
  comments: state.comments.allComments
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Evaluations);
