import React from "react";
import { Route } from "react-router-dom";

// reactstrap components
import { Container } from "reactstrap";

// Login component
import Evaluations from "./Evaluations";

// css
import "../../assets/css/index.css";
import Navbar from "../../components/Navbar";

class Evaluation extends React.Component {
  componentDidMount() {
    document.body.classList.add("gradient");
  }
  componentWillUnmount() {
    document.body.classList.remove("gradient");
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    return (
      <>
        <Navbar />
        <div className="gradient">
          <div className="py-8"></div>
          <Container className="mt--8 pb-5">
            <Evaluations />
          </Container>
        </div>
      </>
    );
  }
}

export default Evaluation;
