import React, { Component } from "react";
import {
  Card,
  CardBody,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import axios from "axios";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import Loading from "../../components/Loader";
import * as EmailValidator from "email-validator";
import { categories } from "../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../utils/mask";
import { upload } from "../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../apis/geolocation";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";
import {
  setLoading,
  setVouchers,
  createVoucher,
  updateVoucher,
  deleteVoucher
} from "../../actions/voucher";
import { setPlaces } from "../../actions/places";
import { loader } from "../../actions/loader";
import { modalOpen, closeModal } from "../../actions/modal";

const types = ["reais", "porcentagem"];

class Voucher extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      voucher: "",
      vouchers: [],
      voucherExpiration: "",
      voucherValue: "",
      voucherType: "",
      voucherQuantity: "",

      partner: ""
    };
  }

  async componentDidMount() {
    await this.props.setLoading();
    await this.props.setPlaces();
    await this.props.setVouchers();
  }

  async componentWillReceiveProps(nextProps) {
    const { vouchers } = nextProps;
    if (vouchers && vouchers.length > 0) {
      this.setState({ vouchers: vouchers });
    }
  }

  deleteVoucher = async id => {
    const vouchers = this.props.vouchers;
    await this.props.deleteVoucher(id, vouchers);
    await this.props.setVouchers();
    return this.notify(`${"success"}`, `${`Voucher deletado com sucesso`}`);
  };

  dropdownToogle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  filterVoucher = text => {
    const input = text.target.value;
    const { vouchers } = this.state;
    const voucherFiltered = _.filter(
      vouchers,
      e =>
        e.title.toLowerCase().includes(input.toLowerCase()) ||
        e.content.toLowerCase().includes(input.toLowerCase())
    );
    this.setState({ voucherFiltered });
  };

  handleToggleModal = content => {
    this.props.modalOpen();
  };

  notify = (param, message) => toast[param](message);

  onChangeHandler = async (event, key, index) => {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    this.setState({ [key]: { file } }, () => {
      reader.onload = async () => {
        let photos = this.state.photos;
        if (key === "folder") {
          this.setState(prevState => ({
            [key]: { file, preview: reader.result }
          }));
        } else {
          photos[index] = reader.result;
          this.setState(prevState => ({
            photos,
            [key]: { file, preview: reader.result }
          }));
        }
      };
    });
    reader.readAsDataURL(file);
  };

  handleDelete = async id => {
    await this.props.deleteVoucher(id, this.state.vouchers);
  };

  vouchersRow = (voucher, index) => {
    if (voucher && voucher.voucher) {
      return (
        <tr key={index}>
          <td>{voucher.place.place}</td>
          <td>{voucher.voucher}</td>
          <td>{voucher.voucherExpiration}</td>
          <td>{voucher.voucherValue}</td>
          <td>{voucher.voucherType}</td>
          <td>{voucher.voucherUsed === "false" ? "Nao" : "Sim"}</td>
          <td>
            <div
              class="ui vertical animated button"
              onClick={() => this.handleDelete(voucher._id)}
              tabindex="0"
            >
              <div class="hidden content">Apagar</div>
              <div class="visible content">
                <i class="trash icon"></i>
              </div>
            </div>
          </td>
        </tr>
      );
    }
  };

  dropdown = () => {
    const { partner, partners } = this.state;
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.dropdownToogle}>
        <DropdownToggle caret className="dropdown-text">
          {partner ? partner : "Selecione"}
        </DropdownToggle>
        <DropdownMenu style={{ flexDirection: "column" }}>
          {this.props.places &&
            this.props.places.map((place, index) => (
              <DropdownItem
                key={index}
                onClickCapture={() =>
                  this.handleDropdown(place.place, place._id)
                }
              >
                {place.place}
              </DropdownItem>
            ))}
        </DropdownMenu>
      </Dropdown>
    );
  };

  typeDropdown = () => {
    const { voucherType } = this.state;
    return (
      <Dropdown
        isOpen={this.state.voucherTypeDropdown}
        toggle={() => this.dropdownToogle("voucher")}
      >
        <DropdownToggle caret className="dropdown-text">
          {voucherType ? voucherType : "Selecione"}
        </DropdownToggle>
        <DropdownMenu style={{ flexDirection: "column" }}>
          {types.map((type, index) => (
            <DropdownItem
              key={index}
              onClickCapture={() => this.handleDropdown(type)}
            >
              {type}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    );
  };

  dropdownToogle = key => {
    if (key === "voucher") {
      return this.setState(prevState => ({
        voucherTypeDropdown: !prevState.voucherTypeDropdown
      }));
    } else {
      this.setState(prevState => ({
        dropdownOpen: !prevState.dropdownOpen
      }));
    }
  };

  handleDropdown = (param, id) => {
    console.log(id);
    if (id) {
      this.setState({
        partner: param,
        partnerId: id,
        dropdownOpen: false,
        voucherTypeDropdown: false
      });
    } else {
      this.setState({
        voucherType: param,
        voucherTypeDropdown: false,
        dropdownOpen: false
      });
    }
  };

  renderModal = () => {
    const { id, folder, title, content } = this.state;

    const isUpdate = id && id.length > 0;

    return (
      <Modal isOpen={this.props.openModal} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>
          {isUpdate ? "Atualizar" : "Novo"} Voucher
        </ModalHeader>
        <ModalBody>
          <div>
            <Row>
              {/* <Col md={5}> */}
              <b>Parceiro</b> {this.dropdown()}
              {/* </Col> */}
              {/* <Col md={5}> */}
              <b>Tipo</b> {this.typeDropdown()}
              {/* </Col> */}
            </Row>
            <Row>
              <Col md={6}>
                <b>Valor</b>
                <Input
                  type="text"
                  placeholder="ex: 10.40, 20.00, 15.99, 16.20"
                  onChange={voucherValue =>
                    this.setState({
                      voucherValue: voucherValue.target.value
                    })
                  }
                  value={this.state.voucherValue}
                />{" "}
              </Col>
              <Col md={6}>
                <b>Quantidade</b>
                <Input
                  type="text"
                  placeholder="ex: 1, 5, 10, 28"
                  onChange={voucherQuantity =>
                    this.setState({
                      voucherQuantity: voucherQuantity.target.value
                    })
                  }
                  value={this.state.voucherQuantity}
                />{" "}
              </Col>
            </Row>
            <b>Validade</b>
            <Input
              type="text"
              placeholder="ex: 01/10/2019"
              onChange={voucherExpiration =>
                this.setState({
                  voucherExpiration: voucherExpiration.target.value
                })
              }
              value={this.state.voucherExpiration}
            />{" "}
          </div>
        </ModalBody>
        <ModalFooter>
          {" "}
          {isUpdate && (
            <Button
              color="danger"
              onClick={() => {
                if (
                  window.confirm(
                    "Atenção, a ação de excluir é irreversível. Você confirma essa ação?"
                  )
                )
                  this.handleDelete(id);
              }}
            >
              Apagar
            </Button>
          )}
          <Button color="secondary" onClick={() => this.handleCancel()}>
            Cancelar
          </Button>{" "}
          <Button color="success" onClick={event => this.validateFields(event)}>
            {isUpdate ? "Atualizar" : "Criar"}
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  saveVoucher = async () => {
    const {
      id,
      partner,
      partnerId,
      voucher,
      voucherExpiration,
      voucherValue,
      voucherType,
      voucherQuantity
    } = this.state;

    const voucherToSave = {
      voucherExpiration,
      voucherValue: parseFloat(voucherValue),
      voucherType,
      voucherQuantity: parseInt(voucherQuantity),
      place: partnerId
    };
    await loader();
    let folderUrl = "";
    console.log(" voucher to save", voucherToSave);
    let isSave = await this.props.createVoucher({ ...voucherToSave });

    isSave && this.resetState();

    window.location.reload();
    this.notify(
      `${isSave ? "success" : "danger"}`,
      `${
        isSave
          ? `Voucher criado com sucesso. Uma notificação foi disparado para todos os usuários do app`
          : "Erro interno de servidor"
      }`
    );
  };

  resetState = () => {
    this.setState({
      id: "",
      voucher: "",
      vouchers: [],
      voucherExpiration: "",
      voucherValue: "",
      voucherType: "",
      voucherQuantity: "",

      partner: ""
    });
  };

  validateFields = async event => {
    const {
      id,
      partner,
      partnerId,
      voucher,
      voucherExpiration,
      voucherValue,
      voucherType,
      voucherQuantity
    } = this.state;

    if (
      !(
        voucherExpiration &&
        voucherExpiration.length > 0 &&
        voucherValue &&
        voucherValue.length > 0 &&
        voucherType &&
        voucherType.length > 0 &&
        voucherQuantity &&
        voucherQuantity.length > 0 &&
        partner &&
        partner.length > 0 &&
        partnerId &&
        partnerId.length > 0
      )
    ) {
      this.notify(
        "error",
        "Verifique se todos os campos foram preenchidos corretamente"
      );
      return;
    }
    return this.saveVoucher();
  };

  handleCancel = () => {
    this.props.closeModal();
    this.resetState();
  };

  handleDelete = id => {
    this.resetState();
    this.deleteVoucher(id);
  };

  render() {
    console.log(this.state);
    const { loading } = this.props;
    const { vouchers, voucherFiltered } = this.state;
    if (loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className="row-1">
            <Row form>
              <Col md={9}>
                <FormGroup>
                  <b className="subtitle">Pesquisar por:</b>
                  <Input
                    name="search"
                    onChange={this.filterVoucher}
                    placeholder="Pesquise aqui por um voucher"
                  />
                </FormGroup>
              </Col>
              <Col md={3}>
                <button
                  className="add-button bt-1"
                  onClick={() => this.handleToggleModal(null)}
                >
                  Adicionar
                </button>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xl={12}>
              <Card>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Estabelecimento</th>
                        <th scope="col">Voucher</th>
                        <th scope="col">Validade</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Usado</th>
                        <th scope="col">Ação</th>
                      </tr>
                    </thead>
                    <tbody>
                      {(voucherFiltered && voucherFiltered.length > 0
                        ? voucherFiltered
                        : vouchers
                      ).map((voucher, index) =>
                        this.vouchersRow(voucher, index)
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.renderModal()}
          <ToastContainer />
        </div>
      );
    }
  }
}

const mapDispatchToProps = {
  setLoading,
  setVouchers,
  createVoucher,
  updateVoucher,
  deleteVoucher,
  setPlaces,
  modalOpen,
  closeModal,
  loader
};

const mapStateToProps = state => ({
  openModal: state.places.openModal,
  loading: state.vouchers.loading,
  vouchers: state.vouchers.vouchers,
  places: state.places.places
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Voucher);
