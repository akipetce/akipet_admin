import React from "react";
import { Route } from "react-router-dom";

// reactstrap components
import { Container } from "reactstrap";

// Login component
import Login from "./Login";

// css
import "../../../assets/css/index.css";

class Auth extends React.Component {
  componentDidMount() {
    document.body.classList.add("gradient");
  }
  componentWillUnmount() {
    document.body.classList.remove("gradient");
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    return (
      <>
        <div className="main-content">
          <div className="py-7 py-lg-8">
            <Container>
              <div className="header-body text-center mb-7"></div>
            </Container>
          </div>
          <Container className="mt--8 pb-5">
            <Login />
          </Container>
        </div>
      </>
    );
  }
}

export default Auth;
