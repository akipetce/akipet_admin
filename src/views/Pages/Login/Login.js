import React from "react";
import { toast, ToastContainer } from "react-toastify";
import { withRouter } from "react-router-dom";
import api from "../../../api";
import { login } from "../../../api/endpoints";
import { TOKEN_KEY, isAuthenticated, setRole } from "../../../api/auth";

import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col
} from "reactstrap";

// REDUX
import { connect } from "react-redux";
import { setUser } from "../../../actions/user";

import "react-toastify/dist/ReactToastify.css";
import "../../../assets/css/index.css";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleLogin = async () => {
    const { email, password } = this.state;
    try {
      const res = await api("post", login, { email, password });
      if (res) {
        if (res.status === 200) {
          console.log("response login", res);
          const token = res.data.token;
          const role = res.data.user.role;
          await isAuthenticated();
          await setRole(role);
          this.props.setUser(res.data.user);
          localStorage.setItem(TOKEN_KEY, token);
          this.props.history.push(
            role === "admin" ? "/places" : "/voucher_validation"
          );
        }
      }
    } catch (err) {
      console.log(err, err.message);
    }
  };

  handleInput = (event, key) => {
    this.setState({
      [key]: event.target.value
    });
  };

  notify = (param, message) => toast[param](message);

  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Faça login com seu Email</small>
              </div>
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="email"
                      onChange={text => this.handleInput(text, "email")}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      onChange={text => this.handleInput(text, "password")}
                    />
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button
                    onClick={this.handleLogin}
                    style={{ backgroundColor: "#00c295", color: "#fff" }}
                    className="my-4"
                    type="button"
                  >
                    Login
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
        <ToastContainer />
      </>
    );
  }
}

const mapDispatchToProps = {
  setUser
};

export default connect(
  null,
  mapDispatchToProps
)(withRouter(Login));
