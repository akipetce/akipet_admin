import React from "react";

// reactstrap components
import { Container } from "reactstrap";

// Login component
import Mobile from "./Mobile";

// css
import "../../../assets/css/index.css";

class MobileContainer extends React.Component {
  componentDidMount() {
    document.body.classList.add("gradient");
  }
  componentWillUnmount() {
    document.body.classList.remove("gradient");
  }
  render() {
    return (
      <>
        <div className="main-content">
          <div className="py-7 py-lg-8">
            <Container>
              <div className="header-body text-center mb-7"></div>
            </Container>
          </div>
          <Container className="mt--8 pb-5">
            <Mobile />
          </Container>
        </div>
      </>
    );
  }
}

export default MobileContainer;
