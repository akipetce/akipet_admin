import React from "react";
import { withRouter } from "react-router-dom";
import { Card, CardBody, Col } from "reactstrap";

import "react-toastify/dist/ReactToastify.css";
import "../../../assets/css/index.css";

class Mobile extends React.Component {
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Por favor acesse esta página em seu computador</small>
              </div>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Mobile;
