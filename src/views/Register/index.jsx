import React from "react";

import { withRouter } from "react-router-dom";

// reactstrap components
import { Container } from "reactstrap";

// Login component
import Register from "./Register";
import RegisterPremium from "./RegisterPremium";

// css
import "../../assets/css/index.css";

class RegisterContainer extends React.Component {
  componentDidMount() {
    document.body.classList.add("gradient");
  }
  componentWillUnmount() {
    document.body.classList.remove("gradient");
  }

  render() {
    console.log("props", this.props);
    const { location } = this.props;
    console.log(location);
    return (
      <>
        <div className="gradient">
          <div className="py-8">
            <img
              alt="logo"
              className="logo-register"
              src={require("../../assets/img/logo.png")}
            />
          </div>
          <Container className="mt--8 pb-5">
            {location.pathname === "/parceiro" ? (
              <Register />
            ) : (
              <RegisterPremium />
            )}
          </Container>
        </div>
      </>
    );
  }
}

export default withRouter(RegisterContainer);
