import React, { Component } from "react";
import {
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import { categories } from "../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../utils/mask";
import * as EmailValidator from "email-validator";
import { upload } from "../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../apis/geolocation";
import { createPartnerPlace } from "../../actions/partnerPlace";
import api from "../../api";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";

class RegisterPremium extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeFiltered: [],
      places: [],
      address: {},
      place: "",
      folder: "",
      number: "",
      description: "",
      timeAttendance: "",
      telephone: "",
      mobile: "",
      email: "",
      cep: "",
      conduction: "",
      specie: "",
      size: "",
      area: "",
      maskedCep: "",
      placeSelected: {},
      latitude: null,
      longitude: null,
      photos: [null, null, null]
    };
  }

  async componentDidMount() {}

  dropdown = () => {
    const { category } = this.state;
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.dropdownToogle}>
        <DropdownToggle caret className="dropdown-text">
          {category ? category : "Selecione"}
        </DropdownToggle>
        <DropdownMenu style={{ flexDirection: "column" }}>
          {categories.map((category, index) => (
            <DropdownItem
              key={index}
              onClickCapture={() => this.handleDropdown(category.section)}
            >
              {category.section}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    );
  };

  dropdownToogle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  handleChange = address => {
    this.setState({ address });
  };

  handleDropdown = param => {
    this.setState({
      category: param,
      dropdownOpen: false
    });
  };

  handleCEP = async () => {
    const { cep, number } = this.state;

    const isValidCEP = cep.length === 10;
    const isValidNumber = number.length > 0;
    if (isValidCEP && isValidNumber) {
      const address = await fetchCEP(this.state.cep, number);
      if (address && Object.values(address).length > 0) {
        this.setState({
          address: address.address,
          latitude: address.geolocation.latitude,
          longitude: address.geolocation.longitude
        });
      } else {
        this.notify("error", "Erro interno de servidor");
      }
    } else {
      this.notify("error", "Por favor preencha CEP e número do endereço");
    }
  };
  notify = (param, message) => toast[param](message);

  onChangeHandler = (event, key, index) => {
    if (event) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        let photos = this.state.photos;
        if (key === "folder") {
          this.setState({ [key]: reader.result });
        } else {
          photos[index] = reader.result;
          this.setState({ photos, [key]: reader.result });
        }
      };
      reader.onerror = function(error) {
        console.log("Error: ", error);
      };
    }
  };

  renderForm = () => {
    const { logradouro, localidade, uf } = this.state.address;
    const {
      id,
      place,
      number,
      description,
      timeAttendance,
      telephone,
      mobile,
      email,
      conduction,
      specie,
      size,
      area
    } = this.state;

    const isUpdate = id && id.length > 0;

    return (
      <div className="register-container">
        <div className="file-upload">
          <div className="image-upload-wrap">
            <input
              className="file-upload-input"
              type="file"
              onChange={file => this.onChangeHandler(file, `folder`)}
              accept="image/*"
            />
            {this.state.folder && this.state.folder.length > 0 ? (
              <img
                className="file-upload-image"
                src={this.state.folder}
                alt="folder"
              />
            ) : (
              <div className="drag-text">
                <h3>Adicione uma imagem</h3>
              </div>
            )}
          </div>
        </div>
        <div>
          <div>
            <b>Nome do local</b>{" "}
            <Input
              type="text"
              placeholder="Nome do estabelecimento"
              onChange={place =>
                this.setState({
                  place: place.target.value
                })
              }
              value={place}
            />{" "}
            <Row form>
              <Col md={4}>
                <b>CEP</b>
                <Input
                  type="text"
                  placeholder="Digite o CEP"
                  onChange={cep =>
                    this.setState({
                      cep: cepMask(cep.target.value)
                    })
                  }
                  value={this.state.cep}
                />{" "}
              </Col>
              <Col md={3}>
                <b>Número</b>
                <Input
                  type="text"
                  placeholder="Número"
                  onChange={number =>
                    this.setState({ number: number.target.value })
                  }
                  value={this.state.number}
                />{" "}
              </Col>
              <Col md={3}>
                <b />
                <button className="add-button bt-2" onClick={this.handleCEP}>
                  Buscar
                </button>
              </Col>
            </Row>
            <b>Endereço</b>{" "}
            <Input
              disabled
              name="address"
              placeholder="Endereço"
              onChange={address => false}
              value={
                logradouro && logradouro.length > 0
                  ? `${logradouro}, ${number} - ${localidade}/${uf}`
                  : ""
              }
            />{" "}
            <b>Descrição</b>
            <Input
              type="textarea"
              placeholder="Breve resumo sobre a atividade do local"
              onChange={description =>
                this.setState({
                  description: description.target.value
                })
              }
              value={description}
            />{" "}
            <Row>
              <Col>
                <b>Horário de atendimento</b>{" "}
                <Input
                  type="text"
                  name="timeAttendance"
                  placeholder="Horário de atendimento"
                  value={timeAttendance}
                  onChange={timeAttendance =>
                    this.setState({
                      timeAttendance: timeAttendance.target.value
                    })
                  }
                />{" "}
              </Col>
              <Col>
                <b>Tipo de local</b> {this.dropdown()}{" "}
              </Col>
            </Row>
            <Row>
              <Col>
                <b>Telefone</b>{" "}
                <Input
                  type="text"
                  name="telephone"
                  placeholder="Telefone para contato"
                  onChange={telephone =>
                    this.setState({
                      telephone: telMask(telephone.target.value)
                    })
                  }
                  value={telephone}
                />
              </Col>
              <Col>
                <b>Celular</b>{" "}
                <Input
                  type="text"
                  name="mobile"
                  placeholder="Celular para contato"
                  onChange={mobile =>
                    this.setState({
                      mobile: mobilMask(mobile.target.value)
                    })
                  }
                  value={mobile}
                />
              </Col>
            </Row>
            <b>E-mail</b>{" "}
            <Input
              type="email"
              min="0"
              placeholder="E-mail"
              onChange={email =>
                this.setState({
                  email: email.target.value
                })
              }
              value={email}
            />{" "}
            <b>Condução</b>{" "}
            <Input
              type="conduction"
              placeholder="coleira, colo, carrinho..."
              onChange={conduction =>
                this.setState({
                  conduction: conduction.target.value
                })
              }
              value={conduction}
            />{" "}
            <b>Espécie</b>{" "}
            <Input
              type="specie"
              placeholder="cachorro, gato..."
              onChange={specie =>
                this.setState({
                  specie: specie.target.value
                })
              }
              value={specie}
            />{" "}
            <b>Porte</b>{" "}
            <Input
              type="size"
              placeholder="pequeno, médio..."
              onChange={size =>
                this.setState({
                  size: size.target.value
                })
              }
              value={size}
            />{" "}
            <b>Área</b>{" "}
            <Input
              type="area"
              placeholder="interna, externa..."
              onChange={area =>
                this.setState({
                  area: area.target.value
                })
              }
              value={area}
            />{" "}
            <b>Imagens do local</b>
            <p>
              É possível inserir apenas três imagens. Caso queira trocar alguma
              imagem basta apenas clicar na imagem anterior e selecionar uma
              nova image{" "}
            </p>
            {this.state.photos.map((photo, index) => (
              <div className="file-upload" key={index}>
                <div className="image-upload-wrap">
                  <input
                    className="file-upload-input"
                    type="file"
                    onChange={file =>
                      this.onChangeHandler(file, `photo${index}`, index)
                    }
                    accept="image/*"
                  />
                  {this.state[`photo${index}`] &&
                  this.state[`photo${index}`].length > 0 ? (
                    <img
                      className="file-upload-image"
                      src={this.state[`photo${index}`]}
                      alt="place"
                    />
                  ) : isUpdate ? (
                    <img
                      className="file-upload-image"
                      src={this.state.photos[index]}
                      alt="place"
                    />
                  ) : (
                    <div className="drag-text">
                      <h3>Adicione uma imagem</h3>
                    </div>
                  )}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="bt-1">
          <Button color="success" onClick={this.validateFields}>
            Enviar
          </Button>
        </div>
      </div>
    );
  };

  savePlace = async () => {
    const {
      id,
      cep,
      place,
      email,
      folder,
      mobile,
      number,
      photos,
      address,
      latitude,
      category,
      longitude,
      conduction,
      specie,
      size,
      area,
      telephone,
      description,
      timeAttendance
    } = this.state;

    const placeToSave = {
      cep,
      email,
      place,
      folder,
      mobile,
      number,
      photos,
      address,
      latitude,
      longitude,
      category,
      telephone,
      description,
      conduction,
      specie,
      size,
      area,
      timeAttendance,
      id: uuid()
    };

    let urls = [];

    await Promise.all(
      [folder, ...photos].map(async (file, index) => {
        const url = await upload(file);
        if (index === 0) {
          urls = [...urls, { folder: url, type: "folder" }];
        }
        urls = [...urls, { photo: url }];
      })
    );

    const isSave = await this.props.createPartnerPlace({
      ...placeToSave,
      photos: urls
    });

    isSave && this.resetState();

    this.notify("success", `Seu estabelecimento foi registrado com sucesso!`);
  };

  resetState = () => {
    this.setState({
      id: "",
      address: {},
      place: "",
      folder: "",
      number: "",
      description: "",
      timeAttendance: "",
      telephone: "",
      mobile: "",
      email: "",
      cep: "",
      conduction: "",
      specie: "",
      size: "",
      area: "",
      placeSelected: "",
      maskedCep: "",
      photo0: null,
      photo1: null,
      photo2: null,
      latitude: null,
      longitude: null,
      photos: [null, null, null]
    });
  };

  validateFields = event => {
    event.preventDefault();

    const {
      email,
      place,
      description,
      timeAttendance,
      telephone,
      mobile,
      photos,
      folder
    } = this.state;

    let isNull = _.filter(photos, e => e === null || e === undefined);

    if (
      !(
        folder &&
        folder.length > 0 &&
        place &&
        place.length > 0 &&
        description &&
        description.length > 10 &&
        timeAttendance &&
        timeAttendance.length > 5 &&
        telephone &&
        telephone.length === 14 &&
        mobile &&
        mobile.length === 15 &&
        !(isNull.lenght > 0)
      )
    ) {
      this.notify(
        "error",
        "Verifique se todos os campos foram preenchidos corretamente"
      );
      return;
    }
    EmailValidator.validate(email)
      ? this.savePlace()
      : this.notify("error", "Insira um e-mail válido");
  };

  render() {
    return (
      <div>
        {this.renderForm()}
        <ToastContainer />
      </div>
    );
  }
}

const mapDispatchToProps = {
  createPartnerPlace
};
const mapStateToProps = state => ({
  openModal: state.places.openModal,
  loading: state.places.loading,
  places: state.places.places
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPremium);
