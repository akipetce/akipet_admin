import React, { Component } from "react";
import {
  Card,
  CardBody,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col
} from "reactstrap";

import _ from "lodash";
import axios from "axios";
import uuid from "uuid/v1";
import { connect } from "react-redux";
import Loading from "../../components/Loader";
import * as EmailValidator from "email-validator";
import { categories } from "../../utils/categories";
import { toast, ToastContainer } from "react-toastify";
import { telMask, mobilMask, cepMask } from "../../utils/mask";
import { upload } from "../../api/firebase";

// utils, api && database
import { fetchCEP } from "../../apis/geolocation";

// css
import "../../assets/css/index.css";
import "../../assets/css/upload.css";
import "../../assets/css/index.css";
import "react-toastify/dist/ReactToastify.css";
import {
  setLoading,
  setPlaces,
  createPlaces,
  updatePlaces,
  deletePlace
} from "../../actions/places";
import { loader } from "../../actions/loader";
import { modalOpen, closeModal } from "../../actions/modal";

const initialState = {
  category: null,
  id: null,
  place: "",
  address: {},
  number: "",
  description: "",
  timeAttendance: "",
  telephone: "",
  mobile: "",
  email: "",
  cep: "",
  conduction: "",
  specie: "",
  size: "",
  area: "",
  maskedCep: "",
  placeSelected: {},
  preview: {},
  latitude: null,
  longitude: null,
  photo0: null,
  photo1: null,
  photo2: null,
  photos: [null, null, null],
  password: "",

  // local loading state
  loading: false
};

class Places extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      placeFiltered: [],
      places: [],
      folder: {}
    };
  }

  async componentDidMount() {
    await this.props.setLoading();
    await this.props.setPlaces();
  }

  async componentWillReceiveProps(nextProps) {
    const { places } = nextProps;
    if (places && places.length > 0) {
      this.setState({ places: places });
    }
  }

  deletePlace = async id => {
    const places = this.props.places;
    await this.props.deletePlace(id, places);
    await this.props.setPlaces();
    this.resetState();
    return this.notify(
      `${"success"}`,
      `${`Estabelecimento deletado com sucesso`}`
    );
  };

  dropdown = () => {
    const { category } = this.state;
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.dropdownToogle}>
        <DropdownToggle caret className="dropdown-text">
          {category ? category : "Selecione"}
        </DropdownToggle>
        <DropdownMenu style={{ flexDirection: "column" }}>
          {categories.map((category, index) => (
            <DropdownItem
              key={index}
              onClickCapture={() => this.handleDropdown(category.section)}
            >
              {category.section}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    );
  };

  dropdownToogle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  filterPlace = text => {
    const input = text.target.value;
    const { places } = this.state;
    const placeFiltered = _.filter(
      places,
      e =>
        e.place.toLowerCase().includes(input.toLowerCase()) ||
        e.category.toLowerCase().includes(input.toLowerCase()) ||
        e.address.logradouro.toLowerCase().includes(input.toLowerCase()) ||
        e.email.toLowerCase().includes(input.toLowerCase()) ||
        e.address.localidade.toLowerCase().includes(input.toLowerCase())
    );
    this.setState({ placeFiltered });
  };

  handleToggleModal = content => {
    if (content) {
      const {
        id,
        cep,
        email,
        place,
        mobile,
        number,
        address,
        category,
        latitude,
        longitude,
        telephone,
        description,
        conduction,
        specie,
        size,
        area,
        folderUrl,
        photosUrl,
        timeAttendance
      } = content;

      const isPhotos = content && content.photo0 && content.photo0.length > 0;

      this.setState({
        id: content._id,
        cep,
        place,
        folder: content.folder,
        number,
        address,
        category,
        description,
        timeAttendance,
        telephone,
        mobile,
        email,
        latitude,
        longitude,
        folderUrl,
        photosUrl,
        conduction,
        specie,
        size,
        area,
        photos: isPhotos ? this.state.photos : [],
        photo0: isPhotos ? content.photo0 : "",
        photo1: isPhotos ? content.photo1 : "",
        photo2: isPhotos ? content.photo2 : ""
      });
    }
    this.props.modalOpen();
  };

  handleChange = address => {
    this.setState({ address });
  };

  handleDropdown = param => {
    this.setState({
      category: param,
      dropdownOpen: false
    });
  };

  handleCEP = async () => {
    const { cep, number } = this.state;

    const isValidCEP = cep.length === 10;
    const isValidNumber = number.length > 0;
    if (isValidCEP && isValidNumber) {
      const address = await fetchCEP(this.state.cep, number);
      if (address && Object.values(address).length > 0) {
        this.setState({
          address: address.address,
          latitude: address.geolocation.latitude,
          longitude: address.geolocation.longitude
        });
      }
      if (address.address.erro) {
        this.notify("error", "CEP Inválido");
        return;
      }
    } else {
      this.notify("error", "Por favor preencha CEP e número do endereço");
    }
  };

  notify = (param, message) => toast[param](message);

  onChangeHandler = async (event, key, index) => {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    this.setState({ [key]: { file } }, () => {
      reader.onload = async () => {
        let photos = this.state.photos;
        if (key === "folder") {
          this.setState(prevState => ({
            [key]: { file, preview: reader.result }
          }));
        } else {
          photos[index] = reader.result;
          this.setState(prevState => ({
            photos,
            [key]: { file, preview: reader.result }
          }));
        }
      };
    });
    reader.readAsDataURL(file);
  };

  placesRow = (place, index) => {
    if (place && place.address) {
      return (
        <tr key={index} onClick={() => this.handleToggleModal(place)}>
          <td>
            <img src={place.folder} alt="folder" className="folder" />
          </td>
          <td>{place.place}</td>
          <td>{`${place.address.logradouro}, ${place.number} - ${place.address.localidade}/${place.address.uf}`}</td>
          <td>{place.category}</td>
          <td>{place.email}</td>
        </tr>
      );
    }
  };

  renderModal = () => {
    const { logradouro, localidade, uf } = this.state.address;
    const {
      id,
      place,
      number,
      description,
      timeAttendance,
      telephone,
      mobile,
      email,
      conduction,
      specie,
      size,
      area,
      password,
      photo0,
      photo1,
      photo2
    } = this.state;

    const isUpdate = id && id.length > 0;
    return (
      <Modal isOpen={this.props.openModal} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>
          {isUpdate ? "Atualizar" : "Novo"} local
        </ModalHeader>
        <div className="file-upload">
          <div className="image-upload-wrap">
            <input
              className="file-upload-input"
              type="file"
              onChange={file => this.onChangeHandler(file, `folder`)}
              accept="image/*"
            />
            {isUpdate ? (
              <img
                className="file-upload-image"
                src={this.state.folder}
                alt="folder"
              />
            ) : this.state.folder &&
              this.state.folder.preview &&
              this.state.folder.preview.length > 0 ? (
              <img
                className="file-upload-image"
                src={this.state.folder.preview}
                alt="folder"
              />
            ) : (
              <div className="drag-text">
                <h3>Adicione uma imagem</h3>
              </div>
            )}
          </div>
        </div>
        <ModalBody>
          <div>
            <b>Nome do local</b>{" "}
            <Input
              type="text"
              placeholder="Nome do estabelecimento"
              onChange={place =>
                this.setState({
                  place: place.target.value
                })
              }
              value={place}
            />{" "}
            <Row form>
              <Col md={4}>
                <b>CEP</b>
                <Input
                  type="text"
                  placeholder="Digite o CEP"
                  onChange={cep =>
                    this.setState({
                      cep: cepMask(cep.target.value)
                    })
                  }
                  value={this.state.cep}
                />{" "}
              </Col>
              <Col md={3}>
                <b>Número</b>
                <Input
                  type="text"
                  placeholder="Número"
                  onChange={number =>
                    this.setState({ number: number.target.value })
                  }
                  value={this.state.number}
                />{" "}
              </Col>
              <Col md={3}>
                <b />
                <button className="add-button bt-2" onClick={this.handleCEP}>
                  Buscar
                </button>
              </Col>
            </Row>
            <b>Endereço</b>{" "}
            <Input
              disabled
              name="address"
              placeholder="Endereço"
              onChange={address => false}
              value={
                logradouro && logradouro.length > 0
                  ? `${logradouro}, ${number} - ${localidade}/${uf}`
                  : ""
              }
            />{" "}
            <b>Descrição</b>
            <Input
              type="textarea"
              placeholder="Breve resumo sobre a atividade do local - Mínimo 50 caracteres"
              onChange={description =>
                this.setState({
                  description: description.target.value
                })
              }
              value={description}
            />{" "}
            <Row>
              <Col>
                <b>Horário de atendimento</b>{" "}
                <Input
                  type="text"
                  name="timeAttendance"
                  placeholder="Horário de atendimento"
                  value={timeAttendance}
                  onChange={timeAttendance =>
                    this.setState({
                      timeAttendance: timeAttendance.target.value
                    })
                  }
                />{" "}
              </Col>
              <Col>
                <b>Tipo de local</b> {this.dropdown()}{" "}
              </Col>
            </Row>
            <Row>
              <Col>
                <b>Telefone</b>{" "}
                <Input
                  type="text"
                  name="telephone"
                  placeholder="Telefone para contato"
                  onChange={telephone =>
                    this.setState({
                      telephone: telMask(telephone.target.value)
                    })
                  }
                  value={telephone}
                />
              </Col>
              <Col>
                <b>Celular</b>{" "}
                <Input
                  type="text"
                  name="mobile"
                  placeholder="Celular para contato"
                  onChange={mobile =>
                    this.setState({
                      mobile: mobilMask(mobile.target.value)
                    })
                  }
                  value={mobile}
                />
              </Col>
            </Row>
            <b>E-mail</b>{" "}
            <Input
              type="email"
              min="0"
              placeholder="E-mail"
              onChange={email =>
                this.setState({
                  email: email.target.value
                })
              }
              value={email}
            />{" "}
            <b>Senha</b>{" "}
            <Input
              type="password"
              placeholder="Mínimo de 6 caracteres"
              onChange={password =>
                this.setState({
                  password: password.target.value
                })
              }
              value={password}
            />{" "}
            <b>Condução</b>{" "}
            <Input
              type="conduction"
              placeholder="coleira, colo, carrinho..."
              onChange={conduction =>
                this.setState({
                  conduction: conduction.target.value
                })
              }
              value={conduction}
            />{" "}
            <b>Espécie</b>{" "}
            <Input
              type="specie"
              placeholder="cachorro, gato..."
              onChange={specie =>
                this.setState({
                  specie: specie.target.value
                })
              }
              value={specie}
            />{" "}
            <b>Porte</b>{" "}
            <Input
              type="size"
              placeholder="pequeno, médio..."
              onChange={size =>
                this.setState({
                  size: size.target.value
                })
              }
              value={size}
            />{" "}
            <b>Área</b>{" "}
            <Input
              type="area"
              placeholder="interna, externa..."
              onChange={area =>
                this.setState({
                  area: area.target.value
                })
              }
              value={area}
            />{" "}
            {this.state.photos && this.state.photos.length > 0 && (
              <>
                <b>Imagens do local</b>
                <p>É possível inserir até três imagens </p>
              </>
            )}
            {this.state.photos &&
              this.state.photos.length > 0 &&
              this.state.photos.map((photo, index) => (
                <div className="file-upload" key={index}>
                  <div className="image-upload-wrap">
                    <input
                      className="file-upload-input"
                      type="file"
                      onChange={file =>
                        this.onChangeHandler(file, `photo${index}`, index)
                      }
                      accept="image/*"
                    />
                    {this.state[`photo${index}`] &&
                    this.state[`photo${index}`].preview &&
                    this.state[`photo${index}`].preview.length > 0 ? (
                      <img
                        className="file-upload-image"
                        src={this.state[`photo${index}`].preview}
                        alt="place"
                      />
                    ) : isUpdate ? (
                      <img
                        className="file-upload-image"
                        src={this.state[`photo${index}`]}
                        alt="place"
                      />
                    ) : (
                      <div className="drag-text">
                        <h3>Adicione uma imagem</h3>
                      </div>
                    )}
                  </div>
                </div>
              ))}
          </div>
        </ModalBody>
        <ModalFooter>
          {" "}
          {isUpdate && (
            <Button
              color="danger"
              onClick={() => {
                if (
                  window.confirm(
                    "Atenção, a ação de excluir é irreversível. Você confirma essa ação?"
                  )
                )
                  this.deletePlace(id);
              }}
            >
              Apagar
            </Button>
          )}
          <Button
            color="secondary"
            onClick={() => {
              this.resetState();
              this.props.closeModal();
            }}
          >
            Cancelar
          </Button>{" "}
          <Button color="success" onClick={event => this.validateFields(event)}>
            {isUpdate ? "Atualizar" : "Criar"}
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  savePlace = async () => {
    const {
      id,
      cep,
      place,
      email,
      folder,
      mobile,
      number,
      photos,
      address,
      latitude,
      category,
      longitude,
      telephone,
      prevPhoto,
      description,
      conduction,
      photo0,
      photo1,
      photo2,
      specie,
      size,
      area,
      password,
      timeAttendance
    } = this.state;

    const isUpdate = id && id.length > 0;
    let isSave = false;
    let urls = [];

    const placeToSave = {
      cep,
      email,
      password,
      place,
      mobile,
      number,
      address,
      latitude: latitude.toString(),
      longitude: longitude.toString(),
      category,
      telephone,
      description,
      conduction,
      specie,
      size,
      area,
      timeAttendance
    };

    await loader();
    let folderUrl = "";

    this.setState({ loading: true });

    if (!isUpdate) {
      let photosToUpload = [];

      [folder, photo0, photo1, photo2].map(photo => {
        if (photo && photo.preview && photo.preview.length > 0) {
          photosToUpload = [...photosToUpload, photo];
        }
      });

      await Promise.all(
        photosToUpload.map(async (file, index) => {
          const url = await upload(file.file);
          if (index === 0) {
            folderUrl = url;
          } else {
            urls = [...urls, url];
          }
        })
      );
    }

    if (isUpdate) {
      isSave = await this.props.updatePlaces(
        placeToSave,
        prevPhoto,
        this.props.places
      );
    } else {
      isSave = await this.props.createPlaces(
        {
          ...placeToSave,
          folder: folderUrl,
          photo0: urls[0] || "",
          photo1: urls[1] || "",
          photo2: urls[2] || ""
        },
        this.props.places
      );
    }

    await this.props.setPlaces();
    isSave && this.resetState();

    this.notify(
      `${isSave ? "success" : "danger"}`,
      `${
        isSave
          ? `Estabelecimento ${
              isUpdate ? "atualizado" : "criado"
            } com sucesso. Uma notificação foi enviada para os usuários do aplicativo`
          : "Erro interno de servidor"
      }`
    );
    this.setState({ loading: false });
  };

  resetState = () => {
    this.setState(initialState);
  };

  validateFields = async event => {
    const {
      id,
      email,
      place,
      description,
      timeAttendance,
      telephone,
      mobile,
      photos,
      folder,
      password,
      photo0,
      photo1,
      photo2
    } = this.state;
    event.preventDefault();

    let isNull =
      id && id.length > 0
        ? []
        : _.filter(photos, e => e === null || e === undefined);

    let isFolderPreview =
      id && id.length > 0
        ? false
        : !(folder && folder.preview && folder.preview.length > 0);

    if (
      !(
        !isFolderPreview &&
        place &&
        place.length > 0 &&
        description &&
        description.length > 10 &&
        timeAttendance &&
        timeAttendance.length > 5 &&
        telephone &&
        telephone.length === 14 &&
        mobile &&
        mobile.length === 15 &&
        !(isNull.lenght > 0)
      )
    ) {
      this.notify(
        "error",
        "Verifique se todos os campos foram preenchidos corretamente"
      );
      return;
    }

    if (password.length < 6) {
      this.notify("error", "Insira uma senha de no mínimo 6 caracteres");
      return;
    }

    EmailValidator.validate(email)
      ? this.savePlace()
      : this.notify("error", "Insira um e-mail válido");
  };

  render() {
    const { loading } = this.props;
    const { places, placeFiltered } = this.state;
    if (loading || this.state.loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className="row-1">
            <Row form>
              <Col md={9}>
                <FormGroup>
                  <b className="subtitle">Pesquisar por:</b>
                  <Input
                    name="search"
                    onChange={this.filterPlace}
                    placeholder="Pesquise aqui por local"
                  />
                </FormGroup>
              </Col>
              <Col md={3}>
                <button
                  className="add-button bt-1"
                  onClick={() => this.handleToggleModal(null)}
                >
                  Adicionar
                </button>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xl={12}>
              <Card>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Folder</th>
                        <th scope="col">Estabelecimento</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">E-mail</th>
                      </tr>
                    </thead>
                    <tbody>
                      {(placeFiltered && placeFiltered.length > 0
                        ? placeFiltered
                        : places
                      ).map((place, index) => this.placesRow(place, index))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.renderModal()}
          <ToastContainer />
        </div>
      );
    }
  }
}

const mapDispatchToProps = {
  setLoading,
  setPlaces,
  createPlaces,
  updatePlaces,
  deletePlace,
  modalOpen,
  closeModal,
  loader
};

const mapStateToProps = state => ({
  openModal: state.places.openModal,
  loading: state.places.loading,
  places: state.places.places
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Places);
