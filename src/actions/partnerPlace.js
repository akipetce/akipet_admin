import {
  LOADING_PROGRESS,
  SET_PARTNER_PLACE_WITH_FAILURE,
  SET_PARTNER_PLACE_WITH_SUCCESS
} from "../actions/actionTypes";
import api from "../api";
import { partner } from "../api/endpoints";

/*
  action for loader
*/
export const setLoading = () => {
  return async dispatch =>
    dispatch({
      type: LOADING_PROGRESS
    });
};

/*
  Create Place at places model
  @place object
*/
export const createPartnerPlace = place => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    let isCreated = false;

    const save = await api("post", partner, place, false);

    isCreated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: SET_PARTNER_PLACE_WITH_FAILURE
      });
    }
    dispatch({
      type: SET_PARTNER_PLACE_WITH_SUCCESS
    });
    return isCreated;
  };
};
