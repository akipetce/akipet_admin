import { LOADING_PROGRESS } from "./actionTypes";

export const loader = () => async dispatch =>
  dispatch({ type: LOADING_PROGRESS });
