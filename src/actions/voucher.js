import {
  LOADING_PROGRESS,
  MODAL_CLOSE,
  FETCH_VOUCHER_SUCCESS,
  FETCH_VOUCHER_FAILURE,
  CREATE_VOUCHER_FAILURE,
  UPDATE_VOUCHER_FAILURE
} from "../actions/actionTypes";
import remove from "lodash/remove";
import api from "../api";
import uniqWith from "lodash/uniqWith";
import isEqual from "lodash/isEqual";
import { voucher_endpoint, push_notification } from "../api/endpoints";
import { pushMessages } from "../utils/push";

/*
  action for loader
*/
export const setLoading = () => {
  return async dispatch =>
    dispatch({
      type: LOADING_PROGRESS
    });
};

/*
  fetch all voucher news on database
*/
export const setVouchers = () => {
  return async dispatch => {
    dispatch({ type: LOADING_PROGRESS });
    const response = await api("get", voucher_endpoint);
    console.log("response", response);
    if (response.status === 200) {
      let voucher = response.data.vouchers;
      console.log("voucher fetch servidor", voucher);
      return dispatch({
        type: FETCH_VOUCHER_SUCCESS,
        payload: voucher
      });
    } else {
      dispatch({
        type: FETCH_VOUCHER_FAILURE,
        payload: {}
      });
    }
  };
};

/*
  Create Voucher at vouchers model
  @place object
*/
export const createVoucher = voucher => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });

    let isCreated = false;

    const save = await api("post", voucher_endpoint, voucher, false);
    const vouchers = await api("get", voucher_endpoint);
    const push = await api(
      "post",
      push_notification,
      pushMessages.newPartner,
      false
    );
    console.log("push", push);
    isCreated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: CREATE_VOUCHER_FAILURE,
        payload: {}
      });
    }
    dispatch({
      type: FETCH_VOUCHER_SUCCESS,
      payload: [...uniqWith(vouchers.data.vouchers, isEqual), voucher]
    });
    return isCreated;
  };
};

/*
   update voucher at vouchers model
   @place object
*/
export const updateVoucher = (voucher, id, vouchers) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });
    console.log("recebi voucher", voucher, id);
    let isUpdated = false;

    const save = await api(
      "put",
      `${voucher_endpoint}/${id}`,
      { ...voucher, voucherUsed: "true" },
      false
    );

    isUpdated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: UPDATE_VOUCHER_FAILURE,
        payload: {}
      });
    }

    const response = await api("get", voucher_endpoint);
    if (response.status === 200) {
      let voucher = response.data.vouchers;
      console.log("voucher fetch servidor", voucher);
      dispatch({
        type: FETCH_VOUCHER_SUCCESS,
        payload: voucher
      });
    }
    return isUpdated;
  };
};

/*
   delete voucher at vouchers model
   @place object
*/
export const deleteVoucher = (id, vouchers) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });
    await api("delete", `${voucher_endpoint}/${id}`);
    await remove(vouchers, e => e._id === id);

    return dispatch({
      type: FETCH_VOUCHER_SUCCESS,
      payload: vouchers
    });
  };
};
