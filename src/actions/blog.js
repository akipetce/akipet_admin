import * as firebase from "firebase";
import {
  LOADING_PROGRESS,
  MODAL_CLOSE,
  FETCH_BLOG_SUCCESS,
  FETCH_BLOG_FAILURE,
  CREATE_BLOG_FAILURE,
  UPDATE_BLOG_FAILURE
} from "../actions/actionTypes";
import remove from "lodash/remove";
import api from "../api";
import uniqWith from "lodash/uniqWith";
import isEqual from "lodash/isEqual";
import { blog_endpoint, push_notification } from "../api/endpoints";
import { pushMessages } from "../utils/push";

/*
  action for loader
*/
export const setLoading = () => {
  return async dispatch =>
    dispatch({
      type: LOADING_PROGRESS
    });
};

/*
  fetch all blog news on database
*/
export const setBlog = () => {
  return async dispatch => {
    dispatch({ type: LOADING_PROGRESS });
    const response = await api("get", blog_endpoint);
    if (response.status === 200) {
      let blog = response.data.blogs;

      return dispatch({
        type: FETCH_BLOG_SUCCESS,
        payload: blog
      });
    } else {
      dispatch({
        type: FETCH_BLOG_FAILURE,
        payload: {}
      });
    }
  };
};

/*
  Create Blog at blogs model
  @place object
*/
export const createBlog = blog => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });

    let isCreated = false;

    const save = await api("post", blog_endpoint, blog, false);
    const blogs = await api("get", blog_endpoint);
    const push = await api(
      "post",
      push_notification,
      pushMessages.olhaAki,
      false
    );
    console.log("push sent", push);

    isCreated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: CREATE_BLOG_FAILURE,
        payload: {}
      });
    }
    dispatch({
      type: FETCH_BLOG_SUCCESS,
      payload: [...uniqWith(blogs.data.blog, isEqual), blog]
    });
    return isCreated;
  };
};

/*
   update blog at blogs model
   @place object
*/
export const updateBlog = (blog, id) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });

    let isUpdated = false;

    const save = await api("put", `${blog_endpoint}/${id}`, blog, false);

    isUpdated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: UPDATE_BLOG_FAILURE,
        payload: {}
      });
    }

    dispatch({
      type: FETCH_BLOG_SUCCESS,
      payload: {}
    });
    return isUpdated;
  };
};

/*
   delete blog at blogs model
   @place object
*/
export const deleteBlog = (id, blogs) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });
    await api("delete", `${blog_endpoint}/${id}`);
    await remove(blogs, e => e._id === id);

    return dispatch({
      type: FETCH_BLOG_SUCCESS,
      payload: blogs
    });
  };
};
