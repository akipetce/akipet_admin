import * as firebase from "firebase";
import {
  LOADING_PROGRESS,
  MODAL_CLOSE,
  FETCH_PLACES_SUCCESS,
  FETCH_PLACES_FAILURE,
  CREATE_PLACES_FAILURE,
  UPDATE_PLACES_FAILURE
} from "../actions/actionTypes";
import remove from "lodash/remove";
import api from "../api";
import uniqWith from "lodash/uniqWith";
import isEqual from "lodash/isEqual";
import {
  places_endpoint,
  create_partner,
  push_notification
} from "../api/endpoints";
import { pushMessages } from "../utils/push";

/*
  action for loader
*/
export const setLoading = () => {
  return async dispatch =>
    dispatch({
      type: LOADING_PROGRESS
    });
};

/*
  fetch all places on database
*/
export const setPlaces = () => {
  return async dispatch => {
    dispatch({ type: LOADING_PROGRESS });
    const places = await api("get", places_endpoint);

    if (places && places.data) {
      let allPlaces = places.data.places;

      return dispatch({
        type: FETCH_PLACES_SUCCESS,
        payload: allPlaces
      });
    } else {
      dispatch({
        type: FETCH_PLACES_FAILURE,
        payload: {}
      });
    }
  };
};

/*
  Create Place at places model
  @place object
*/
export const createPlaces = (place, allPlaces) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });

    let isCreated = false;

    const save = await api("post", places_endpoint, place, false);
    if (save && save.post && save.post.status === 200) {
      const user = await api("post", create_partner, {
        email: place.email,
        password: place.password,
        place: save.post.data.places._id,
        role: "partner"
      });
      console.log("user", user);
      const push = await api(
        "post",
        push_notification,
        pushMessages.newPartner,
        false
      );
      // if (push.post && push.post.status === 200) {
      //   console.log("push response", push);
      // }
    }

    const places = await api("get", places_endpoint);

    isCreated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: CREATE_PLACES_FAILURE,
        payload: {}
      });
    }

    dispatch({
      type: FETCH_PLACES_SUCCESS,
      payload: [...uniqWith(places.data.places, isEqual), place]
    });
    return isCreated;
  };
};

/*
   update place at places model
   @place object
*/
export const updatePlaces = (nextPlace, prevPlace) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });

    let isUpdated = false;

    const place = { ...nextPlace, photos: prevPlace };

    const save = await api(
      "put",
      `${places_endpoint}/${nextPlace._id}`,
      place,
      false
    );

    isUpdated = save.post && save.post.status === 200;

    if (!save.post) {
      return dispatch({
        type: UPDATE_PLACES_FAILURE,
        payload: {}
      });
    }

    dispatch({
      type: FETCH_PLACES_SUCCESS,
      payload: {}
    });
    return isUpdated;
  };
};

/*
   delete place at places model
   @place object
*/
export const deletePlace = (id, places) => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await dispatch({ type: MODAL_CLOSE });
    await api("delete", `${places_endpoint}/${id}`);
    await remove(places, e => e._id === id);

    return dispatch({
      type: FETCH_PLACES_SUCCESS,
      payload: places
    });
  };
};
