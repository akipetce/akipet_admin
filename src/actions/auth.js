import { LOGIN_AUTHENTICATION_SUCCESS } from "./actionTypes";

export const authenticate = data => {
  return {
    type: LOGIN_AUTHENTICATION_SUCCESS,
    payload: data
  };
};
