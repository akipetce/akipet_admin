import { SET_USER, REMOVE_USER } from "./actionTypes";

export const setUser = user => async dispatch => {
  return dispatch({
    type: SET_USER,
    payload: user
  });
};

export const removeUser = () => async dispatch =>
  dispatch({
    type: REMOVE_USER
  });
