import { MODAL_OPEN, MODAL_CLOSE } from "./actionTypes";

export const modalOpen = () => {
  return {
    type: MODAL_OPEN
  };
};

export const closeModal = () => {
  return {
    type: MODAL_CLOSE
  };
};
