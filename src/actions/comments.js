import * as firebase from "firebase";
import {
  LOADING_PROGRESS,
  FETCH_COMMENTS_SUCESS,
  FETCH_COMMENTS_FAILURE,
  DELETE_COMMENT_SUCESS
} from "../actions/actionTypes";
import remove from "lodash/remove";
import api from "../api";
import axios from "axios";
import uniqWith from "lodash/uniqWith";
import isEqual from "lodash/isEqual";
import { comments } from "../api/endpoints";

/*
  action for loader
*/
export const setLoading = () => {
  return async dispatch =>
    dispatch({
      type: LOADING_PROGRESS
    });
};

/*
  fetch all comments on database
*/
export const setComments = () => {
  return async dispatch => {
    dispatch({ type: LOADING_PROGRESS });
    const response = await api("get", comments);
    console.log(response);
    if (response.status === 200) {
      let allComments = response.data.comments;
      return dispatch({
        type: FETCH_COMMENTS_SUCESS,
        payload: allComments
      });
    } else {
      dispatch({
        type: FETCH_COMMENTS_FAILURE,
        payload: {}
      });
    }
  };
};

/*
   delete comment at comments model
   @place object
*/
export const deleteComment = id => {
  return async dispatch => {
    await dispatch({ type: LOADING_PROGRESS });
    await api("delete", `${comments}/${id}`);
    return dispatch({
      type: DELETE_COMMENT_SUCESS
    });
  };
};
